Laboratorium #A
===============

1. Stworzyć aplikację do `rozkładu liczby na czynniki pierwsze <http://www.math.edu.pl/czynniki-pierwsze>`__.
   W celu przyspieszenia operacji wykorzystać memcache'a dla obliczonych liczb
   i wyników pośrednich. Do przetestowania użyć ciągu kilkuset liczb.


2. Używając zadań lub kolejek do uruchomienia procesów na innych maszynach
   posortować listę losowych liczb wygenerowanych za pomocą
   `API z random.org <http://www.random.org/clients/http/>`__
